package testRun;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import loginPage.HomePage;
import loginPage.SignInPage;
import loginPage.SignUpPage;

public class AssignmentTwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.way2automation.com/demo.html");
		driver.manage().window().maximize();
		
		String parent_URL=driver.getWindowHandle();
		Set<String> list=driver.getWindowHandles();
		
		Iterator<String> iterator=list.iterator();
		
		while (iterator.hasNext()){
			String child_URL=iterator.next();
			
			if(!parent_URL.equals(child_URL)){
				driver.switchTo().window(child_URL);
				HomePage test1=new HomePage(driver);
				test1.selectDraggableBox();
				
				SignUpPage test2=new SignUpPage(driver);
				test2.clickSignIn();
				
				SignInPage test3=new SignInPage(driver);
				test3.enterUserNameSignPage("filimon4321");
				test3.enterPasswordSignPage("filimon1234");
				test3.clickSubmitButton();
			}
		} driver.switchTo().window(parent_URL);
		
		//Find the number of box in the page
		List<WebElement> countAllBox=driver.findElements(By.xpath("//*[@id='wrapper']/div[2]/div[2]/div/ul/li"));
		int totalBoxNumbers=countAllBox.size();
		System.out.println(totalBoxNumbers);
		
		//Find the number of Box under the Interaction
		List<WebElement> interactionNumberBox=driver.findElements(By.xpath("//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li"));
		int boxNumbers=interactionNumberBox.size();
		System.out.println(interactionNumberBox); 
	}
}
