package testRun;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import boxPage.DropDownBox;
import loginPage.*;

public class dropDownRun {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.way2automation.com/demo.html");
		driver.manage().window().maximize();
		
		HomePage test1=new HomePage(driver);
		test1.selectDraggableBox();
		
		String parent_URL=driver.getWindowHandle();
		Set<String> list=driver.getWindowHandles();
		
		Iterator<String> iterator=list.iterator();
		
		while (iterator.hasNext()){
			String child_URL=iterator.next();
			
			if(!parent_URL.equals(child_URL)){
				driver.switchTo().window(child_URL);
								
				SignUpPage test2=new SignUpPage(driver);
				test2.clickSignIn();
				
				SignInPage test3=new SignInPage(driver);
				test3.enterUserNameSignPage("filimon4321");
				test3.enterPasswordSignPage("filimon1234");
				test3.clickSubmitButton();
			}
		} driver.switchTo().window(parent_URL);
		
		DropDownBox test4=new DropDownBox(driver);
		test4.selectDropDownBox();
		test4.clickEnterCountryTab();
		test4.typeCountryName("Et", "Ethiopia");
		
	}

}
