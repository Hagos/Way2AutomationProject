package testRun;

import java.util.Iterator;
import java.util.Set;

/* Author:      Filimon Hagos
 * Date :       4/7/2016 
 * Description: Sign up an account and log in to the page
 * 
 */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import loginPage.HomePage;
import loginPage.SignUpPage;

public class AssignmentOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver=new FirefoxDriver();
		driver.get("http://www.way2automation.com/demo.html");
		driver.manage().window().maximize();
		
		String parent_URL=driver.getWindowHandle();
		Set<String> list=driver.getWindowHandles();
		
		Iterator<String> iterator=list.iterator();
		
		while (iterator.hasNext()){
			String child_URL=iterator.next();
			
			if(!parent_URL.equals(child_URL)){
				driver.switchTo().window(child_URL);
				HomePage test1=new HomePage(driver);
				test1.selectDraggableBox();
				
				SignUpPage test2=new SignUpPage(driver);
				test2.typeName("Fil");
				test2.typePhoneNumber("2025695585");
				test2.typeEmail("filimon1234@gmail.com");
				test2.typeCity("Ethiopia");
				test2.typeUserName("filimon4321");
				test2.typePassword("filimon1234");
				test2.clickSubmit();
				
				driver.close();
			}
		}
		driver.switchTo().window(parent_URL);
	}
	

}
