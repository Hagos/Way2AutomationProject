package boxPage;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DropDownBox {
//koko
	// i got it
	WebDriver driver;
	public DropDownBox(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[4]/ul/li[2]/a/figure")
	WebElement dropdownBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[4]/ul/li[2]/a/figure/img")
	WebElement countryDropDownbox;
	
	@FindBy(xpath="//*[@id='wrapper']/div/div[1]/div[1]/ul/li[2]/a")
	WebElement enterCountryTab;
	
	@FindBy(linkText="Enter Country")
	WebElement selectCountryTab;
	
	@FindBy(xpath="//body/select")
	WebElement selectCountryDropDown;
	
	@FindBy(xpath="//div[@class='ui-widget']/select")
	WebElement typeCountryField;
	
	public void selectDropDownBox(){
		dropdownBox.click();
	}
	public void clickCountryDropDown(){
		countryDropDownbox.click();
	}
	
	public String clickSelectCountryDropDown(String countryName){
		Select select=new Select(selectCountryDropDown);
		select.selectByValue(countryName);
		return countryName;
	}
	public void typeCountryName(String partialCountryName, String actualCountryNname){
		
		typeCountryField.clear();
		
		String partialName=partialCountryName;
		String actualName=actualCountryNname;
		typeCountryField.sendKeys(partialName);

		
		
		List<WebElement> results=driver.findElements(By.xpath("/*[@id='ui-id-1']/li"));
		int resultSize=results.size();
		
		for(int i=0;i<resultSize;i++){
			System.out.println(results.get(i).getText());
		}
		
		for(WebElement result:results){
			if(partialCountryName.equals(actualCountryNname)){
				result.click();
			}
		}
			
	}
	
	public void clickEnterCountryTab(){
		enterCountryTab.click();
	}
	
	public void clickSelectCountryTab(){
		selectCountryTab.click();
	}
}
