package loginPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class SignUpPage {

	WebDriver driver;
	public SignUpPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
		

	@FindBy(name="name")
	WebElement nameField;
	
	@FindBy(name="phone")
	WebElement phoneField;
	
	@FindBy(name="email")
	WebElement emailField;
	
	@FindBy(name="country")
	WebElement countryChoice;
	
	@FindBy(name="city")
	WebElement cityField;
	
	@FindBy(xpath="//*[@id='load_box']/form/fieldset[6]/input")
	WebElement userNameField;
	
	@FindBy(xpath="//*[@id='load_box']/form/fieldset[7]/input")
	WebElement passwordField;
	
	@FindBy(xpath="//*[@id='load_box']/form/div/div[1]/p/a")
	WebElement signInButton;
	
	@FindBy(xpath=".//*[@id='load_box']/form/div/div[2]/input")
	WebElement submitButton;
	
	public void clickSignIn(){
		signInButton.click();
	}
	
	public void clickSubmit(){
		submitButton.click();
	}
	
	public void typeUserName(String userName){
		userNameField.sendKeys(userName);
	}
	
	public void typePhoneNumber(String phone){
		phoneField.sendKeys(phone);
	}
	
	public void typeEmail(String email){
		emailField.sendKeys(email);
	}
	
	public void typeCity(String city){
		cityField.sendKeys(city);
	}
	public String selectCountry(String country){
		Select select=new Select(countryChoice);
		select.selectByVisibleText(country);
		return country;
	}
	
	public void typeName(String name){
		nameField.sendKeys(name);
	}
	
	public void typePassword(String password){
		passwordField.sendKeys(password);
	}
	
}
