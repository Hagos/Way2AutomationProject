package loginPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {

	WebDriver driver;
	public SignInPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[@id='login']/form/fieldset[1]/input")
	WebElement userNameSignPage;
	
	@FindBy(xpath="//*[@id='login']/form/fieldset[2]/input")
	WebElement passwordSignPage;
	
	@FindBy(xpath="//*[@id='login']/form/div/div[1]/p/a")
	WebElement signUpLink;
	
	@FindBy(xpath="//*[@id='login']/form/div/div[2]/input")
	WebElement submitButton;
	
	public String enterUserNameSignPage(String userName){
		userNameSignPage.sendKeys(userName);
		return userName;
	}
	
	public String enterPasswordSignPage(String password){
		passwordSignPage.sendKeys(password);
		return password;
	}
	
	public void clickSignUpButton(){
		signUpLink.click();
	}
	
	public void clickSubmitButton(){
		submitButton.click();
	}
	
}
