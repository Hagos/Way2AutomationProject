package loginPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	WebDriver driver;
	public HomePage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Interaction Section
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li[1]/a/figure/img")
	WebElement draggableBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li[2]/a/figure/img")
	WebElement droppableBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li[3]/a/figure/img")
	WebElement resizableBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li[4]/a/figure/img")
	WebElement selectableBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li[5]/a/figure/img")
	WebElement sortableBox;
	
	//Widget Section
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[1]/ul/li[1]/a/figure/img")
	WebElement accordinBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[2]/ul/li[2]/a/figure/img")
	WebElement autocompleteBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[2]/ul/li[3]/a/figure/img")
	WebElement datepickerBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[2]/ul/li[4]/a/figure/img")
	WebElement menuBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[2]/ul/li[5]/a/figure/img")
	WebElement sliderBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[2]/ul/li[6]/a/figure/img")
	WebElement tabsBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[2]/ul/li[7]/a/figure/img")
	WebElement tooltipBox;	
	
	//Frame and Windows Section
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[3]/ul/li/a/figure/img")
	WebElement frameAndWindowsBox;
	
	//Dynamic Elements Section
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[4]/ul/li[1]/a/figure/img")
	WebElement submitButtonClickBox;
	
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[4]/ul/li[2]/a/figure/img")
	WebElement dropDownBox;
	
	//Regisration Section
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[5]/ul/li/a/figure/img")
	WebElement registrationBox;
	
	//Alert Section
	@FindBy(xpath="//*[@id='wrapper']/div[2]/div[2]/div[6]/ul/li/a/figure")
	WebElement alertBox;
	

	public void selectDraggableBox(){
		draggableBox.click();
	}
	public void selectDroppableBox(){
		droppableBox.click();
	}
	public void selectResizableBox(){
		resizableBox.click();
	}
	public void selectSelectableBox(){
		selectableBox.click();
	}
	public void selectSortableBox(){
		sortableBox.click();
	}
	public void selectAccordingBox(){
		accordinBox.click();
	}
	public void selectAutocompleteBox(){
		autocompleteBox.click();
	}
	public void selectDatePickerBox(){
		datepickerBox.click();
	}
	public void selectMenuBox(){
		menuBox.click();
	}
	public void selectSliderBox(){
		sliderBox.click();
	}
	public void selectTabsBox(){
		tabsBox.click();
	}
	public void selectToolTipBox(){
		tooltipBox.click();
	}
	public void selectFrameAndWindowsBox(){
		frameAndWindowsBox.click();
	}
	public void selectSubmitButtonClickBox(){
		submitButtonClickBox.click();
	}
	public void selectDropDownBox(){
		dropDownBox.click();
	}
	public void selectRegistrationBox(){
		registrationBox.click();
	}
	public void selectAlertBox(){
		alertBox.click();
	}
}
